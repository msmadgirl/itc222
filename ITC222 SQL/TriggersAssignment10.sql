USE MetroAlt;

--Create a trigger to fire when an employee is assigned 
--to a second shift in a day. Have it write to an overtime 
--table. the Trigger should create the overtime table if 
--it doesn't exist. Add an employee for two shifts to test the trigger.